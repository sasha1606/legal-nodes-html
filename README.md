## Todo

- send data to server form modal ??
- show error ???

## If you need open window modal without js

- add attr style="display: block;" to 'modal-window' node (for example: <div class="modal-window forgot-password" id="forgot-password" style="display: block;">)
- add attr style="display: block;" to 'modal-window__overlay' node (for example: <div class="modal-window__overlay js-close-modal" style="display: block;"></div>)
- add class 'is-active' to 'modal-window__content' node (for example:  <form class="modal-window__content is-active">)

