$(document).ready(function(){

    $('.js-close-modal').on('click', function () {
        ModalMethods.closeModal($(this))
    });

    $('.js-toggle-visibility-password').on('click', function () {
        ModalMethods.toggleVisibilityPassword($(this))
    });

    $('.js-open-subscribe-area-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('subscribe-area-modal')
    });

    $('.js-open-contact-us-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('contact-us-modal')
    });

    $('.js-open-forgot-password-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('forgot-password-modal')
    });

    $('.js-open-set-new-password-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('set-new-password-modal')
    });

    $('.js-open-product-hunt-modal-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('product-hunt-modal')
    });

    $('.js-open-sign-up-modal-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('sign-up-modal')
    });

    $('.js-open-sign-in-modal-modal').on('click', function (e) {
        e.preventDefault();
        ModalMethods.openModal('sign-in-modal')
    });
});



const ModalMethods = {
    openModal: (id) => {
        $('.modal-window')
            .find('.modal-window__content')
            .removeClass('is-active');
        $('.modal-window')
            .find('.modal-window__overlay')
            .fadeOut();
        $('.modal-window').fadeOut(0, () => {
            $('#' + id).css('display', 'block');
            $('#' + id + ' .modal-window__overlay').fadeIn('slow');
            $('#' + id + ' .modal-window__content').addClass('is-active');
        });
    },

    closeModal: (elem) => {
        // const target = event instanceof Node ? $(event) : $(event.target),
        //     signUpModal = target.closest('#sign-up'),
        //     contactLawyerModal = target.closest('#contact-lawyer');
        //
        // if (target[0].classList.contains('modal-window__overlay') && signUpModal.length && window.location.pathname.indexOf('/survey') !== -1) {
        //     return false;
        // } else if (target[0].classList.contains('modal-window__overlay') && contactLawyerModal.length) {
        //     return false;
        // }else {
            // console.log(target);

            elem
                .closest('.modal-window')
                .find('.modal-window__content')
                .removeClass('is-active');

            elem
                .closest('.modal-window')
                .find('.modal-window__overlay')
                .fadeOut(450, () => {
                    elem.closest('.modal-window').fadeOut(0);
                });
        // }
    },

    toggleVisibilityPassword: (elem) => {
        if (
            elem
                .closest('.form-group')
                .hasClass('is-visible')
        ) {
            elem
                .closest('.form-group')
                .removeClass('is-visible')
                .find('input')
                .attr('type', 'password');
        } else {
            elem
                .closest('.form-group')
                .addClass('is-visible')
                .find('input')
                .attr('type', 'text');
        }
    },

    updateInputs: () => {
        $('input, select').each((eq, item) => {
            const inputValue = $(item).val();
            if (inputValue !== '') {
                $(item)
                    .parents('.form-group')
                    .addClass('is-focused');
            }
        });
    },
};