document.addEventListener("DOMContentLoaded", function(event) {

    // hide loader
    document.body.classList.add("is-loaded");

    document.querySelector('.js-header-burger').addEventListener('click',() => {
        document.body.classList.toggle('is-menu');
    });
});