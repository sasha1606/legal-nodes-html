document.addEventListener("DOMContentLoaded", function(event) {

    const gdprParams = {
        questionBlockNodes: document.querySelectorAll('.js-gdpr-question-block'),
        questionNextBtnNode: document.querySelector('.js-gdpr-question-next'),
        questionPrevBtnNode: document.querySelector('.js-gdpr-question-prev'),
        questionCurrentIndexNode: document.querySelector('.js-gdpr-question-current-index'),
        questionProgressNode: document.querySelector('.js-gdpr-question-progress'),
        questionFormNode: document.querySelector('.js-gdpr-question-form'),
        questionCount: document.querySelector('.js-gdpr-question-count').dataset['questionCount'],
        questionWrap: document.querySelector('.js-gdpr-question-wrap'),
        questionSteps: document.querySelectorAll('.js-gdpr-question-steps'),
        registrationWrap: document.querySelector('.js-gdpr-registration-wrap'),
    };

    const gdprInstance = new Gdpr(gdprParams);

    gdprInstance.init();
});


class Gdpr {

    static activeQuestionPartClass = 'gdpr-question-part_active';
    static activeQuestionStepClass = 'is-active';
    static dataActiveQuestionName = 'questionName';

    constructor(
        {
            questionBlockNodes,
            questionNextBtnNode,
            questionPrevBtnNode,
            questionCurrentIndexNode,
            questionProgressNode,
            questionFormNode,
            questionCount,
            questionWrap,
            questionSteps,
            registrationWrap,
        }) {
        this.questionBlockNodes = [ ...questionBlockNodes];
        this.questionNextBtnNode = questionNextBtnNode;
        this.questionPrevBtnNode = questionPrevBtnNode;
        this.questionProgressNode = questionProgressNode;
        this.questionCurrentIndexNode = questionCurrentIndexNode;
        this.questionFormNode = questionFormNode;
        this.questionCount = questionCount;
        this.questionWrap = questionWrap;
        this.questionSteps = questionSteps;
        this.registrationWrap = registrationWrap;
    }

    init () {
        this.toggleDisabledNextBtn();
        this.toggleDisabledPrevBtn();

        this.initListener();
    };

    initListener () {
        this.questionNextBtnNode.addEventListener('click', (e) => {
            e.preventDefault();
            this.next();

        });

        this.questionPrevBtnNode.addEventListener('click', (e) => {
            e.preventDefault();
            this.prev();
        });

        this.questionFormNode.addEventListener('click', (event) => {
            const target = event.target;

            if (target.tagName.toLocaleLowerCase() != 'input') {
                this.questionNextBtnNode.disabled = false;
            }
        });
    }

    next () {
        const indexActiveQuestionBlockNode = this.getIndexActiveQuestionBlockNode();

        if (indexActiveQuestionBlockNode + 1 === +this.questionCount) {

            this.finishedAnsweringQuestions();
            return false;
        }

        const nextActiveIndexBlockQuestion = indexActiveQuestionBlockNode + 2;
        this.updateUIPart(nextActiveIndexBlockQuestion);

        this.toggleDisabledPrevBtn();
        this.toggleDisabledNextBtn();
    }

    prev () {

        const indexActiveQuestionBlockNode = this.getIndexActiveQuestionBlockNode();

        if (indexActiveQuestionBlockNode + 1 === 1 ) {
            return false;
        }

        this.updateUIPart(indexActiveQuestionBlockNode);

        this.toggleDisabledNextBtn();
        this.toggleDisabledPrevBtn();
    }

    toggleDisabledNextBtn () {

        const activeQuestionBlockNode = this.getActiveQuestionBlockNode();

        const activeQuestionName =  activeQuestionBlockNode.dataset[Gdpr.dataActiveQuestionName];
        const questionInputs = activeQuestionBlockNode.querySelectorAll(`input[name="${activeQuestionName}"]`);

        const isAnyInputsChecked = [...questionInputs].some((item) => item.checked);

        this.questionNextBtnNode.disabled = !isAnyInputsChecked;
    }

    toggleDisabledPrevBtn () {

        let isQuestionPrevBtnDisabled = true;

        const currentQuestionIndex = this.getIndexActiveQuestionBlockNode() + 1;

        if (currentQuestionIndex > 1) {
            isQuestionPrevBtnDisabled = false
        }

        this.questionPrevBtnNode.disabled = isQuestionPrevBtnDisabled;

    }

    updateUIPart (nextIndex) {
        const activeQuestionBlockNode = this.getActiveQuestionBlockNode();

        // question block
        activeQuestionBlockNode.classList.remove(Gdpr.activeQuestionPartClass);
        this.questionBlockNodes[nextIndex - 1].classList.add(Gdpr.activeQuestionPartClass);


        // update current question index
        this.questionCurrentIndexNode.innerHTML = nextIndex;

        // update progress
        const progressWidth = 100 / this.questionCount * nextIndex + "%";
        this.questionProgressNode.style.width = progressWidth;
    }


    getActiveQuestionBlockNode () {
        return this.questionBlockNodes.find((item) => item.classList.contains(Gdpr.activeQuestionPartClass));
    }

    getIndexActiveQuestionBlockNode () {
        return this.questionBlockNodes.findIndex((item) => item.classList.contains(Gdpr.activeQuestionPartClass));

    }

    finishedAnsweringQuestions () {
        this.sendAnswersForCalculate().then(() => {
            this.hiddenQuestionBlock();
            this.displayRegisterBlock();
        });
    }

    async sendAnswersForCalculate () {
        const checkedInput = this.questionFormNode.querySelectorAll('input:checked');
        const inputValue = [...checkedInput].map((item) => item.value);

        const pagType = document.querySelector('.js-pag-type').value;
        const formUrl = document.querySelector('.js-form-url').value;

        const formData = new FormData();
        formData.set("type", pagType);
        formData.set("answers", inputValue.join());


        return await fetch(formUrl, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: formData,
        });
    }

    hiddenQuestionBlock () {
        this.questionWrap.classList.add('hidden')
    }

    displayRegisterBlock () {
        this.registrationWrap.classList.remove('hidden');
        this.setActiveStep(2);
    }

    setActiveStep(step) {
        this.questionSteps[step-1].classList.add(Gdpr.activeQuestionStepClass);
    };
}