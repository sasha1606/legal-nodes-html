$(document).ready(function(){

    // hide loader
    document.body.classList.add("is-loaded");

    // scroll header
    window.addEventListener('scroll', addClassWhenHeaderScroll);

    smoothScrollingHash();

    $('.js-home-areas-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToScroll: 1,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    });


    $('.js-home-lawyers-slider').slick({
        dots: true,
        infinite: true,
        speed: 400,
        slidesToScroll: 1,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 4000,
                settings: 'unslick',
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    });


    $('.js-home-trust-clients-slider').slick({
        dots: true,
        infinite: true,
        speed: 400,
        slidesToScroll: 1,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    });
    
    
    function smoothScrollingHash() {
        $('.js-scroll-anchor').on('click', function() {
            if (window.location.href === '/' ) {
                const hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 500);

                return false;
            }
        });
    }

    function addClassWhenHeaderScroll() {
        const headerNode = $('.js-header');

        const viewportWidth = window.innerWidth;
        if (viewportWidth < 992) return false;

        const offsetToTop = document.body.getBoundingClientRect().top;
        const positiveOffsetToTop = Math.abs(offsetToTop);
        const headerScrolling = headerNode.hasClass('header_scrolling');

        if (positiveOffsetToTop > 50 && !headerScrolling) {
            headerNode.addClass('header_scrolling');
            headerNode.removeClass('header_not-scrolling');
        } else if (positiveOffsetToTop <= 50 && headerScrolling) {
            headerNode.addClass('header_not-scrolling');
            headerNode.removeClass('header_scrolling');
        }
    }

});

