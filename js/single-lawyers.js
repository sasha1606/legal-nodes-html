document.addEventListener("DOMContentLoaded", function(event) {


    const accordionWrapNode = document.getElementsByClassName('js-single-lawyers-accordion-wrap');
    const accordionDescriptionWrapNode = document.getElementsByClassName('js-accordion-description-wrap');

    const accordionAttributeId = 'data-single-lawyer-accordion-id';
    const accordionDescriptionAttributeId = 'data-single-lawyer-accordion-description-id';

    for (let i = 0; i < accordionWrapNode.length; i++) {
        accordionWrapNode[i].addEventListener('click', toggleAccordion);
    }

    function toggleAccordion (event) {
        if(!event.target.hasAttribute(accordionAttributeId)) {
            return false;
        }

        const accordionId = event.target.getAttribute(accordionAttributeId);

        const accordionDescriptionItemNode = document.querySelectorAll(`[${accordionDescriptionAttributeId}]`);
        const selectedAccordionDescriptionItemNode = document.querySelectorAll(`[${accordionDescriptionAttributeId}='${accordionId}']`);

        for (let i = 0; i < accordionDescriptionItemNode.length; i++) {
            if (!accordionDescriptionItemNode[i].classList.contains('hidden')) {
                accordionDescriptionItemNode[i].classList.add('hidden');
            }
        }

        for (let i = 0; i < selectedAccordionDescriptionItemNode.length; i++) {
            selectedAccordionDescriptionItemNode[i].classList.remove('hidden');
        }

        document.querySelector('.accordion-item__children-list-item_active').classList.remove('accordion-item__children-list-item_active');


        event.target.classList.add('accordion-item__children-list-item_active');
    }


    // collapse
    const collapseBtnNode = document.getElementsByClassName('js-single-lawyer-collapse-btn');

    for (let i = 0; i < collapseBtnNode.length; i++) {
        collapseBtnNode[i].addEventListener('click', collapse);
    }

    function collapse (event) {
        const parentNode = event.target.closest('.js-single-lawyer-collapse');
        const isCollapse = parentNode.getAttribute('data-collapse');
        const isCollapseBool = isCollapse === 'true';

        parentNode.setAttribute('data-collapse', `${!isCollapseBool}`);

        let btnText = event.target.getAttribute('data-btn-text-short');

        if (isCollapseBool) {
            btnText = event.target.getAttribute('data-btn-text-full');
        }

        event.target.innerText = btnText;
    }

});
