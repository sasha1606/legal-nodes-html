$(document).ready(function(){

    const postContentNode  = document.querySelector('.js-single-post-content');
    const offsetTopToShowShare = 200;
    const offsetBottomToHideShare = 300;

    const handleScroll = () => {
        const viewportWidth = window.innerWidth;
        if (viewportWidth < 1200 || !postContentNode) return false;

        const documentFullHeight = document.body.offsetHeight;
        const viewportHeight = window.innerHeight;
        const documentHeightWithoutOneViewport = documentFullHeight - viewportHeight;

        const documentOffsetToTop = document.body.getBoundingClientRect().top;
        const documentPositiveOffsetToTop = Math.abs(documentOffsetToTop);

        const postContentOffsetToTop = postContentNode.getBoundingClientRect().top;

        if (
            postContentOffsetToTop < offsetTopToShowShare &&
            documentPositiveOffsetToTop < documentHeightWithoutOneViewport - offsetBottomToHideShare
        ) {
            $('.js-single-post-share-wrap').addClass('single-post__share-wrap_display');
        } else if (
            postContentOffsetToTop >= offsetTopToShowShare ||
            documentPositiveOffsetToTop >= documentHeightWithoutOneViewport - offsetBottomToHideShare
        ) {
            $('.js-single-post-share-wrap').removeClass('single-post__share-wrap_display');
        }
    };


    window.addEventListener('scroll', handleScroll);
});
